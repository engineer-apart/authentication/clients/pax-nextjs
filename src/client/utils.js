/* eslint-disable no-restricted-globals */

import nextCookie from 'next-cookies';
import cookie from 'js-cookie';

// Used to deduct from calculations of expiry tiem
const NEGATION_OFFSET_IN_SECONDS = 300;

// Sets client credentials
export const setClientConfig = (config) => {
  global.credentials = config;
};

// Gets client credentials (used for things outside the React renderer)
export const getClientConfig = () => (
  global.credentials
);

// given a timestamp, determines how many milliseconds are still left, with a negation offset
const intervalExpiryTime = (timeStamp, negation = 0) => (
  Date.parse(timeStamp) - Date.now() - negation * 1000
);

// Determines if a token has expired
export const hasTokenExpired = ({ expiryTime }) => (
  Boolean(!(expiryTime && Math.max(0, intervalExpiryTime(expiryTime, NEGATION_OFFSET_IN_SECONDS))))
);

// Proper test to determine if browser
export const isBrowser = () => (typeof window !== 'undefined');

// Login function for browser cookie
export const loginBrowser = (user, overWrite = true) => {
  console.log('In login function, setting the user cookie to --->', user);
  if (isBrowser() && user) {
    // do not over write user cookie if it exists
    if (!overWrite && cookie.get('user')) {
      return;
    }
    const { expiryTime } = user;
    if (!expiryTime) throw new Error('No expiryTime for user auth token');
    cookie.set('user', user, { expires: new Date(expiryTime) });
  }
};

// Logout function. This will just clear the user cookie, however there
// are two ways the cookie can be set: with and without a domain. If set
// with a domain, it needs to be present to remove the cookie. This
// function will blindly try both ways
export const logout = () => {
  const domain = cookie.get('auth-domain-cookie');
  if (domain) {
    cookie.remove('user', { domain });
    cookie.remove('auth-domain-cookie', { domain });
  }
  cookie.remove('user');
  const logoutEvent = new CustomEvent('logout');
  window.dispatchEvent(logoutEvent);

  // Why local storage? This will sync things in other tabs
  // (see https://stackoverflow.com/questions/5370784/localstorage-eventlistener-is-not-called/6846158#answer-6846158)
  window.localStorage.setItem('logout', Date.now());
};

// Obtains user from the cookie (server and client)
// Note: The ctx argument is for use inside the getInitialProps
// functions. It is set (anti-pattern) on the function level
// inside the getInitialProperties function. This is so that the
// same interface can be used on client and server
export const getUser = () => {
  // getUser.ctx set in getInitialProps in withAuth
  try {
    const user = getUser.ctx && !isBrowser() ? nextCookie(getUser.ctx).user : cookie.get('user');
    if (user === 'undefined' || !user) return { };
    return JSON.parse(user);
  } catch (error) {
    console.error(`Error: ${error}`); // eslint-disable-line no-console
  }
  return { };
};

// Obtains the token associated with the user found in the cookie
export const getUserToken = () => {
  const { token } = getUser();
  return token;
};

// Removes hash from history (clean up)
export const removeFragmentFromUrl = () => {
  if (!isBrowser()) return; // only valid for browser
  if (window.location.hash.length === 0) return; // there is no fragment to remove

  // Check if the browser supports history.replaceState.
  if (history.replaceState) {
    // Keep the exact URL up to the hash.
    const cleanHref = window.location.href.split('#')[0];

    // Replace the URL in the address bar without messing with the back button.
    history.replaceState(null, null, cleanHref);
  } else {
    // Well, you're on an old browser, we can get rid of the _=_ but not the #.
    window.location.hash = '';
  }
};

// Extracts user information from location hash
export const extractUserFromFragment = () => {
  if (!isBrowser()) return undefined;

  try {
    const user = window.location.hash.slice(1);
    console.log('Trying to extract user from hash', user);
    // if we are able to parse json, then this is a valid security token
    // TODO: check for a specific key as well
    return user.length ? JSON.parse(window.atob(user)) : undefined;
  } catch (error) { } // eslint-disable-line no-empty

  // user could not be found
  return undefined;
};

/* eslint-enable no-restricted-globals */
