import React from 'react';
import {
  logout,
  getUserToken,
  withUserCredentials,
  AuthLoginLink,
  refreshToken,
} from '@engineerapart/pax-nextjs';

const LoginLinks = () => (
  <React.Fragment>
    <br />
    <AuthLoginLink redirectTo="http://localhost:3000" strategy="google">Google Login</AuthLoginLink>
    <br />
    <AuthLoginLink redirectTo="http://dev.tenantree.com:3000" strategy="facebook">Facebook Login</AuthLoginLink>
  </React.Fragment>
);

class homePage extends React.Component {
  static async getInitialProps() {
    console.log('The token called from getInitialProps ---->', getUserToken());
  }

  state = {
    userToken: getUserToken(),
  }

  refreshUserToken() {
    refreshToken()
      .then(() => {
        this.setState({ userToken: getUserToken() });
      });
  }

  render() {
    const { user } = this.props;
    const { userToken } = this.state;

    return (
      <div>
        {user.name ? `Welcome ${user.name}, your token is\n${userToken}` : 'You are not logged in'}
        {user.name ? <button type="button" onClick={() => logout()}>Logout</button> : <LoginLinks />}
        <div>
          {user.name ? <button type="button" onClick={() => this.refreshUserToken()}>Refresh User Token</button> : null }
        </div>
      </div>
    );
  }
}

export default withUserCredentials(homePage);
