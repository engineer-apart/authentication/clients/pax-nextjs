import React from 'react'; // eslint-disable-line import/no-unresolved
import { UserCredentialsContext } from './withUserCredentials';
import { ClientConfigContext } from './authLoginLink';

import {
  getUser,
  loginBrowser,
  extractUserFromFragment,
  removeFragmentFromUrl,
  hasTokenExpired,
  setClientConfig,
} from './utils';

import {
  refreshToken,
} from './refreshToken';

// Extracts info necessary to render React tree. This
// does not contain any token info (which will change when)
// being refreshed. Use "getUserToken" function to get token
const extractUserInfo = (user = { }) => {
  const { id, email, picture, name } = user;
  return { id, email, picture, name };
};

// Why this component? Because it will allow for shallow rendering meaning that
// when user changes, it's children (in this case the APP) won't re-render the
// entire tree since it's props probably don't change
const UserCredentialsContextProvider = ({ value, children }) => (
  <UserCredentialsContext.Provider value={value}>{children}</UserCredentialsContext.Provider>
);

export default config => (App) => {
  const { clientCredentials } = config;
  setClientConfig(clientCredentials);
  return (
    class WithAuth extends React.Component {
      static displayName = 'withAuth(App)';

      state = { user: { } };

      static async getInitialProps({ Component, router, ctx }) {
        // Warning: This is probably an anti-pattern (setting a variable on the function)
        // The reason why I am doing this is because it allows for the same interface
        // to work on the client and server. This is because the server needs ctx
        getUser.ctx = ctx;
        let user = getUser(); // next try get it from cookie
        const { expiryTime } = user;

        // Refresh user's token if it has expired
        if (expiryTime && hasTokenExpired(user)) {
          user = await refreshToken(user);
        }

        const appProps = App.getInitialProps
          ? await App.getInitialProps({ Component, router, ctx: { ...ctx, user: extractUserInfo(user) } })
          : { };

        return { ...appProps, user };
      }

      constructor(props) {
        super(props);
        this.syncLogout = this.syncLogout.bind(this);

        // User state is embedded in the fragment by auth service, and will be
        // needed if the auth domain is different to the app's domain. If they
        // are the same base domain, then the user cookie is set by the server,
        // however if different, the fragment will be used to log the user in
        if (extractUserFromFragment()) {
          loginBrowser(extractUserFromFragment(), false);
          removeFragmentFromUrl();
        }

        // Try to obtain user from props, else from cookie
        const { user } = this.props;
        this.state.user = (user && Object.keys(user).length) ? user : getUser();
      }

      componentDidMount() {
        // Register events
        window.addEventListener('logout', this.syncLogout);
        window.addEventListener('storage', this.syncLogout);
      }

      componentWillUnmount() {
        window.removeEventListener('logout', this.syncLogout);
        window.removeEventListener('storage', this.syncLogout);

        // Why local storage? This will sync things in other tabs
        // (see https://stackoverflow.com/questions/5370784/localstorage-eventlistener-is-not-called/6846158#answer-6846158)
        window.localStorage.removeItem('logout');
      }

      syncLogout(event) {
        // When logging out, need to rerender, so state is changed
        if (event.type === 'logout' || event.key === 'logout') {
          this.setState({ user: { } });
        }
      }

      render() {
        const { user: userProp, url, ...props } = this.props;
        const { user } = this.state;

        return ( // eslint-disable-next-line react/destructuring-assignment
          <ClientConfigContext.Provider value={config}>
            <UserCredentialsContextProvider value={user}>
              <App {...props} />
            </UserCredentialsContextProvider>
          </ClientConfigContext.Provider>
        );
      }
    }
  );
};
