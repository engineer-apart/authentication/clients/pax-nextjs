import React from 'react'; // eslint-disable-line import/no-unresolved

export const ClientConfigContext = React.createContext({});

// TODO: This must change to the prod URL when it's available
// export const AUTH_URL = 'https://auth.engineerapart.com';
// export const AUTH_URL = 'https://auth.tenantree.com';
const strategies = ['facebook', 'google'];

const AuthLoginLink = ({ children, redirectTo, strategy }) => {
  if (!strategies.includes(strategy)) {
    throw new Error(`The auth strategy '${strategy}' is not supported`);
  }
  const redirect = redirectTo ? `&redirect=${redirectTo}` : '';

  return (
    <ClientConfigContext.Consumer>
      {({ clientCredentials: { clientId }, authUrl }) => (
        <a href={`${authUrl}?authStrategy=${strategy}${redirect}&clientId=${clientId}`}>
          {children}
        </a>
      )}
    </ClientConfigContext.Consumer>
  );
};

export default AuthLoginLink;
