import {
  hasTokenExpired,
  isBrowser,
  loginBrowser,
  logout,
  getUser,
  getClientConfig,
} from './utils';

// Requests a user token be refreshed
export const refreshToken = async (user) => {
  try {
    const { authUrl } = getClientConfig();
    const { expiryTime, token } = await fetch(`${authUrl}/refresh`, { method: 'post', body: JSON.stringify({ token: user.token }) }).then(res => res.json());
    // Will force user to reauthenticate, unless called in getInitialProps, in which
    // case it will just render as if user is not logged in
    if (!expiryTime || !token) return { };

    const refreshedUser = { ...user, token, expiryTime };
    loginBrowser(refreshedUser); // This will be applicable for client (browser)
    return refreshedUser;
  } catch (error) {
    return { };
  }
};

// Redirects to an auth URL
const redirectToLoginStrategy = (clientId, strategy, authUrl) => {
  if (!isBrowser()) return; // only intended on client
  const URL = `${authUrl}?authStrategy=${strategy}&redirect=${window.location}&clientId=${clientId}`;
  window.location.replace(URL);
};

const logoutAndRedirectToLoginStrategy = ({ strategy }) => {
  logout(); // remove the current cookie
  const { clientCredentials: { clientId }, authUrl } = getClientConfig();
  redirectToLoginStrategy(clientId, strategy, authUrl);
};

export default async () => {
  const user = getUser();
  if (Object.keys(user).length === 0) {
    console.log('No user is considered logged in, so refresh will exit');
    return; // user is not logged in
  }

  // If user not logged and token has not expired, then
  // this is a no-op
  if (user && hasTokenExpired(user)) {
    try {
      await refreshToken(user);
    } catch (error) {
      console.log(`Error trying to refresh token: ${error}`);
      logoutAndRedirectToLoginStrategy(user);
    }
  }
};
