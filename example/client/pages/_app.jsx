import React from 'react';
import App, { Container } from 'next/app';
import { withAuth } from '@engineerapart/pax-nextjs';

class MyApp extends App {
  static async getInitialProps({ Component, ctx }) {
    let pageProps = { };

    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx);
    }

    return { pageProps };
  }

  render() {
    const { Component, pageProps } = this.props;

    return (
      <Container>
        <Component {...pageProps} />
      </Container>
    );
  }
}

const config = {
  clientCredentials: {
    clientId: 'y9k4NJI51rxAy5wVzzFH',
  },
  authUrl: 'https://auth.tenantree.com',
};

export default withAuth(config)(MyApp);
